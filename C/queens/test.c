#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	time_t t;

	srand(time(&t));

	for (int i = 0; i < 10; ++i)
	{
		printf("%d\n", random() % 10);
	}
}