#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WIDTH 8
#define HEIGHT 8
#define QUEEN_NUM 8

#define TRUE 1
#define FALSE 0

void GenerateBoard(int** board);
void InitBoard(int** board);
void PrintBoard(int** board);
int PlaceQueen(int** board, int x, int y);
int ValidateBoard(int** board);

int main()
{
	int board[HEIGHT][WIDTH];
	time_t seed;

	srand(time(&seed));

	GenerateBoard(board);
	PrintBoard(board);
	return 0;
}

void GenerateBoard(int** board)
{
	int i = 0;
	int x,y;

	do{
		InitBoard(board);

		for (i = 0; i < QUEEN_NUM; ++i)
		{
			do{
				x = rand() % WIDTH;
				y = rand() % HEIGHT;
			}while(!PlaceQueen(board, x, y));
		}
		//PrintBoard(board);
	}while(!ValidateBoard(board));
	
}

int PlaceQueen(int** board, int x, int y)
{
	if(board[x][y])
	{
		return FALSE;
	}

	board[x][y] = 1;

	return TRUE;
}

void InitBoard(int** board)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < HEIGHT; ++i)
	{
		board[i] = (int*)malloc(sizeof(int) * WIDTH);
		for (j = 0; j < WIDTH; ++j)
		{
			board[i][j] = 0;
		}
	}
}

int ValidateBoard(int** board)
{
	int i = 0;
	int j = 0;
	int count_row = 0;
	int count_column = 0;

	//Check rows
	for (i = 0; i < HEIGHT; ++i)
	{
		count_row = count_column = 0;
		for (j = 0; j < WIDTH; ++j)
		{
			count_row += board[i][j];
			count_column += board[j][i];
		}
		if(count_row > 1 || count_column > 1)
		{
			return FALSE;
		}
	}

	return TRUE;
}

void PrintBoard(int** board)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < HEIGHT; ++i)
	{
		printf("+---+---+---+---+---+---+---+---+\n");
		for (j = 0; j < WIDTH; ++j)
		{
			if(board[i][j])
			{
				printf("| Q ");
			}
			else
			{
				printf("|   ");	
			}
		}
		printf("|\n");
	}
	printf("+---+---+---+---+---+---+---+---+\n");
}