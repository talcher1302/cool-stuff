#include <stdlib.h>
#include <stdio.h>

void func();

int main()
{
	func();
	printf("address of me is %X and of func is %X\n", main, func);
	return 0;
}

void func()
{
	static int* a;
	int i = 0;
	a = &i;
	
	printf("Addr %X\n", a);
	
	for(i = 0; i < 0xFFFFF; i++)
	{
		if((void*)(main + i) == &a)
			printf("Found %X\n", (main + i));
	}
}